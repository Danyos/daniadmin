<?php

use App\Models\Admin\School\LessonTimeFolder;
use Illuminate\Database\Seeder;

class LessonTimeFolderSeeder extends Seeder
{
    public function run()
    {
        $Folder = [
            [

            'title' => 'Խումբ 1',
            'type' => 'offline',
            'user_id' => 1,
            'created_at' => '2019-04-15 19:13:32',
            'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 2',
                'type' => 'offline',
                'user_id' => 1,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 3',
                'type' => 'offline',
                'user_id' => 1,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 4',
                'type' => 'offline',
                'user_id' => 1,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 5',
                'type' => 'online',
                'user_id' => 3,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 6',
                'type' => 'online',
                'user_id' => 2,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Խումբ 7',
                'type' => 'online',
                'user_id' => 2,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Անհատական',
                'type' => 'offline',
                'user_id' => 2,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
            [
                'title' => 'Անհատական',
                'type' => 'online',
                'user_id' => 1,
                'created_at' => '2019-04-15 19:13:32',
                'updated_at' => '2019-04-15 19:13:32',
            ],
        ];

        LessonTimeFolder::insert($Folder);
    }
}
