<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentPriceListMathsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_price_list_maths', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id');
            $table->integer('parent_id')->nullable();
            $table->enum('status',['active','inactive'])->default('inactive')->comment('Վարձավճարի մուծել է թե ոչ');

            $table->string('price')->nullable()->comment('Վարձավճարի մուծել է մի մաս');
            $table->string('price_full')->nullable()->comment('Վարձավճարի մուծել է ');
            $table->bigInteger('day')->nullable();
            $table->date('start_date')->nullable()->comment('Ամսվա մուծման օրվա սկիզբ');
            $table->date('end_date')->nullable()->comment('Վարձավճարի ավարտի է մի մաս');
            $table->foreign('student_id')->references('id')->on('students_models');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_price_list_maths');
    }
}
