@extends('admin.layouts.admin')
@section('content')

    <?php
    $settings=true;
    $routerPathCreted="admin.users.create";
    $routerPathedit="admin.users.edit";
    $this_id=$user->id;
    $section_menu="Օգտատեր";

    $section_menu_url="admin.users.index";
    $section_menu_section_show="Դիտել";
    ?>

    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <table class="table table-bordered table-striped" style="color: white;">
            <tbody>
                <tr>
                    <th>
                        {{ trans('global.user.fields.name') }}
                    </th>
                    <td>
                        {{ $user->name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.email') }}
                    </th>
                    <td>
                        {{ $user->email }}
                    </td>
                </tr>
                <tr>
                    <th>
                        {{ trans('global.user.fields.email_verified_at') }}
                    </th>
                    <td>
                        {{ $user->email_verified_at }}
                    </td>
                </tr>
                <tr>
                    <th>
                        Roles
                    </th>
                    <td>
                        @foreach($user->roles as $id => $roles)
                            <span class="label label-info label-many">{{ $roles->title }}</span>
                        @endforeach
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

@endsection
