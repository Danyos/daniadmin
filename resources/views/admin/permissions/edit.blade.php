@extends('admin.layouts.admin')
@section('content')

    <?php


    $settings=true;
    $routerPathCreted="admin.permissions.create";
    $routerPathshow="admin.permissions.show";
    $this_id=$permission->id;
    $section_menu="Թույլատվություն";
    $section_menu_url="admin.permissions.index";
    $section_menu_section_edit="Փոփոխել";
    ?>
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

        <form action="{{ route("admin.permissions.update", [$permission->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('global.permission.fields.title') }}*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($permission) ? $permission->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.permission.fields.title_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
