<nav id="sidebar">
    <div class="shadow-bottom"></div>
    <ul class="list-unstyled menu-categories" id="accordionExample">
        <li class="menu">
            <a href="#dashboard" data-active="{{ request()->is('admin') || request()->is('admin/') ? 'true' : '' }}" data-toggle="collapse" aria-expanded="{{ request()->is('admin') || request()->is('admin/') ? 'true' : '' }}" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                    <span>Գլխավոր</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled {{ request()->is('admin') || request()->is('admin/') ? 'show' : '' }}" id="dashboard" data-parent="#accordionExample">
                <li class="{{ request()->is('admin') || request()->is('admin/') ? 'active' : '' }}">
                    <a href="index-2.html"> Անալիտկա </a>
                </li>
                <li>
                    <a href="{{route('admin.LessonTimeList.index')}}"> Վարձավճարներ </a>
                </li>
                <li>
                    <a href="index2.html"> Այլ ծախսեր </a>
                </li>
            </ul>
        </li>

        <li class="menu">
            <a href="#app" data-toggle="collapse" aria-expanded="{{ request()->is('admin/notes') || request()->is('admin/noes/*') ? 'true' : '' }}" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-cpu"><rect x="4" y="4" width="16" height="16" rx="2" ry="2"></rect><rect x="9" y="9" width="6" height="6"></rect><line x1="9" y1="1" x2="9" y2="4"></line><line x1="15" y1="1" x2="15" y2="4"></line><line x1="9" y1="20" x2="9" y2="23"></line><line x1="15" y1="20" x2="15" y2="23"></line><line x1="20" y1="9" x2="23" y2="9"></line><line x1="20" y1="14" x2="23" y2="14"></line><line x1="1" y1="9" x2="4" y2="9"></line><line x1="1" y1="14" x2="4" y2="14"></line></svg>
                    <span>Բաժիներ</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled {{ request()->is('admin/notes') || request()->is('admin/noes/*') ? 'show' : '' }}" id="app" data-parent="#accordionExample">
                <li>
                    <a href="index-2.html"> Կարծիք </a>
                </li>
                <li>
                    <a href="index2.html"> Աստղ </a>
                </li>
                <li>
                    <a href="apps_mailbox.html"> Mailbox  </a>
                </li>

                <li class="{{ request()->is('admin/notes') || request()->is('admin/noes/*') ? 'active' : '' }}">
                    <a href="{{route('admin.notes.index')}}"> Թղթապանակ </a>
                </li>
                <li>
                    <a href="apps_scrumboard.html">Օրացույց</a>
                </li>
                <li>
                    <a href="apps_scrumboard.html">Ըստ սանդղակի</a>
                </li>

                </li>
            </ul>
        </li>

        <li class="menu">
            <a href="#components" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-box"><path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path><polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline><line x1="12" y1="22.08" x2="12" y2="12"></line></svg>
                    <span>Դասընթացներ</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="components" data-parent="#accordionExample">
                <li>
                    <a href="component_accordion.html"> Մեր ուսանողները  </a>
                </li>
                <li>
                    <a href="apps_todoList.html">Այսօրվա գրանցվածներ </a>
                </li>

                <li>
                    <a href="component_modal.html"> Ընթացքի մեջ </a>
                </li>
                <li>
                    <a href="component_cards.html"> Ընդհանուր </a>
                </li>
                <li>
                    <a href="component_bootstrap_carousel.html">Թեկնածուներ</a>
                </li>

            </ul>
        </li>

        <li class="menu">
            <a href="#elements" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-zap"><polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2"></polygon></svg>
                    <span>Վճարումներ</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="elements" data-parent="#accordionExample">

                <li>
                    <a href="element_tooltips.html"> Վճարած </a>
                </li>
                <li>
                    <a href="element_treeview.html"> Անվճար </a>
                </li>
                <li>
                    <a href="element_typography.html"> Լրացած </a>
                </li>
            </ul>
        </li>

        <li class="menu">
            <a href="fonticons.html" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-target"><circle cx="12" cy="12" r="10"></circle><circle cx="12" cy="12" r="6"></circle><circle cx="12" cy="12" r="2"></circle></svg>
                    <span>Աշխատակազմ</span>
                </div>
            </a>
        </li>

        <li class="menu">
            <a href="widgets.html" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-airplay"><path d="M5 17H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-1"></path><polygon points="12 15 17 21 7 21 12 15"></polygon></svg>
                    <span>Blog</span>
                </div>
            </a>
        </li>

        <li class="menu">
            <a href="table_basic.html" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layout"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="3" y1="9" x2="21" y2="9"></line><line x1="9" y1="21" x2="9" y2="9"></line></svg>
                    <span>Հաղորդգրություներ</span>
                </div>
            </a>
        </li>

        <li class="menu">
            <a href="#datatables" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-layers"><polygon points="12 2 2 7 12 12 22 7 12 2"></polygon><polyline points="2 17 12 22 22 17"></polyline><polyline points="2 12 12 17 22 12"></polyline></svg>
                    <span>Ծառայություներ</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="datatables" data-parent="#accordionExample">
                <li>
                    <a href="table_dt_basic.html"> Ծառայություներ </a>
                </li>
                <li>
                    <a href="table_dt_basic-light.html"> Գանցված անձնակազմ </a>
                </li>

            </ul>
        </li>

        <li class="menu">
            <a href="#forms" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
                    <span>Մեր մասին</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="forms" data-parent="#accordionExample">
                <li>
                    <a href="form_bootstrap_basic.html"> Հիմնական </a>
                </li>
                <li>
                    <a href="form_input_group_basic.html"> Տեղեկատվական</a>
                </li>
            </ul>
        </li>
        @can('user_management_access')
        <li class="menu">
            <a href="#users" data-toggle="collapse" aria-expanded="{{ request()->is('admin/users') || request()->is('admin/users/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'true' : '' }}" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
                    <span>Օգտատեր</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled  {{ request()->is('admin/users') || request()->is('admin/users/*') || request()->is('admin/roles') || request()->is('admin/roles/*') || request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'show' : '' }}" id="users" data-parent="#accordionExample">
                <li  class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                    <a href="{{route('admin.permissions.index')}}"> Թույլատվություն </a>
                </li>
                <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                    <a href="{{route('admin.roles.index')}}"> Պաշտոն </a>
                </li>
                <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                    <a href="{{route('admin.users.index')}}"> Օգտատեր </a>
                </li>
            </ul>
        </li>
        @endcan
        <li class="menu">
            <a href="#pages" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                    <span>Էջեր</span>
                </div>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>
                </div>
            </a>
            <ul class="collapse submenu list-unstyled" id="pages" data-parent="#accordionExample">
                <li>
                    <a href="pages_helpdesk.html"> Slider </a>
                </li>
                <li>
                    <a href="pages_contact_us.html"> Կատարած աշ․ </a>
                </li>
                <li>
                    <a href="pages_faq.html"> Ինչու սովորել ծ </a>
                </li>

                <li>
                    <a href="pages_privacy.html"> Privacy Policy </a>
                </li>
                <li>
                    <a href="pages_coming_soon.html"> Coming Soon </a>
                </li>
                <li>
                    <a href="#pages-error" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> Դասընթացներ <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg> </a>
                    <ul class="collapse list-unstyled sub-submenu" id="pages-error" data-parent="#pages">
                        <li>
                            <a href="pages_faq2.html"> Դասընթացներ </a>
                        </li>
                        <li>
                            <a href="pages_error500.html"> Դասավանդողներ </a>
                        </li>
                        <li>
                            <a href="pages_error503.html"> - Հատուկ Դասընթացներ </a>
                        </li>
                        <li>
                            <a href="pages_maintenence.html"> - Գումարային </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>



    </ul>
    <!-- <div class="shadow-bottom"></div> -->

</nav>
