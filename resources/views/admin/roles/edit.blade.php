@extends('admin.layouts.admin')
@section('css')
    <link href="{{asset('admin_style/assets/css/scrollspyNav.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin_style/plugins/select2/select2.min.css')}}">
    @endsection
@section('js')

    <script src="{{asset('admin_style/plugins/select2/select2.min.js')}}"></script>

    <script>
        $(".tagging").select2({
            tags: true
        });

    </script>
    <script>
        $("#selectAll").click(function(){

            $('#permissions').select2('destroy').find('option').prop('selected', 'selected').end().select2();

        });

        $("#DeselectAll").click(function(){
            $('#permissions').select2('destroy').find('option').prop('selected', '').end().select2();

        });
    </script>
    @endsection
@section('content')
    <?php
    $settings=true;
    $routerPathCreted="admin.roles.create";
    $routerPathshow="admin.roles.show";
    $this_id=$role->id;
    $section_menu="Պաշտոն";
    $section_menu_url="admin.roles.index";
    $section_menu_section_edit="Փոփոխել";

    ?>
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

        <form action="{{ route("admin.roles.update", [$role->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title">{{ trans('global.role.fields.title') }}*</label>
                <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($role) ? $role->title : '') }}">
                @if($errors->has('title'))
                    <em class="invalid-feedback">
                        {{ $errors->first('title') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.role.fields.title_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('permissions') ? 'has-error' : '' }}">
                <label for="permissions">{{ trans('global.role.fields.permissions') }}*
                    <span class="btn btn-info btn-xs select-all" id="selectAll" >Select all</span>

                    <span class="btn btn-info btn-xs deselect-all" id="DeselectAll">Deselect all</span></label>
                <select name="permissions[]" id="permissions" class="form-control tagging" multiple="multiple">
                    @foreach($permissions as $id => $permissions)
                        <option value="{{ $id }}" {{ (in_array($id, old('permissions', [])) || isset($role) && $role->permissions->contains($id)) ? 'selected' : '' }}>
                            {{ $permissions }}
                        </option>
                    @endforeach
                </select>
                @if($errors->has('permissions'))
                    <em class="invalid-feedback">
                        {{ $errors->first('permissions') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('global.role.fields.permissions_helper') }}
                </p>
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>

@endsection
