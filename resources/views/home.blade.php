@extends('admin.layouts.admin')
@section('content')
    <div id="content" class="main-content">
        <div class="layout-px-spacing">

            <div class="row layout-top-spacing">

                <div class="col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-one">
                        <div class="widget-heading">
                            <h5 class="">Ամսվա գումար</h5>
                            <ul class="tabs tab-pills">
                                <li><a href="javascript:void(0);" id="tb_1" class="tabmenu">Monthly</a></li>
                            </ul>
                        </div>

                        <div class="widget-content">
                            <div class="tabs tab-content">
                                <div id="content_1" class="tabcontent">
                                    <div id="revenueMonthly"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-two">
                        <div class="widget-heading">
                            <h5 class="">Տիրապետելու մակարդակը</h5>
                        </div>
                        <div class="widget-content">
                            <div id="chart-2" class=""></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
                    <div class="widget widget-chart-two">
                        <div class="widget-heading">
                            <h5 class="">Ուսանողները</h5>
                        </div>
                        <div class="widget-content">
                            <div id="donut-chart" class=""></div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12 layout-spacing">
                    <div class="widget-two">
                        <div class="widget-content">
                            <div class="w-numeric-value">
                                <div class="w-content">
                                    <span class="w-value">Հիմնական ծախսեր</span>
                                    <span class="w-numeric-title">Ամսվա ընթացքում.</span>
                                </div>
                                <div class="w-icon">
                                    220000 դրամ
                                </div>
                            </div>
                            <div class="w-chart">
                                <div id="s-bar" class=""></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
@section('js')
    <script src="{{asset('admin_style/assets/js/dashboard/dash_1.js')}}"></script>

    <script>
        // Ամբողջ եկամուտ
        @isset($data)
        var series_all = @json($data);

        var series_clean = [250000, 100000];
        var series_price = '{{$allprice}}  Դրամ';
        @endisset
        //Աշխատոների աշխատավարձ

        var employee = ['Lena', 'Mushegh', 'Hayk', 'Mariam'];
        var employee_amount = ['70000', '30000', '60000', '60000'];

        // Մեր աշակերտները
        var students = [3, 4, 2];
        // Գրանցված ուսանողներ
        var registerstudents = [44, 55, 41, 17, 60]
    </script>

@endsection
