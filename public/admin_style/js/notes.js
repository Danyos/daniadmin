
$('#btn-add-notes').on('click', function (event) {
    $('#notesMailModal').modal('show');
    $('#btn-n-save').hide();
    $('#btn-n-add').show();
})

function submitform() {
    var title = document.getElementById('n-title').value
    var desc = document.getElementById('n-description').value
    var date = document.getElementById('n-date').value
    var result = validate(title, desc)
    if (result == true) {
        var data = [
            {
                'title': title,
                'desc': desc,
                'date': date,
            }

        ];
        save(data)
    }
}
function validate(title, desc) {
    var result = false;
    if (title.length > 1) {
        document.getElementById('validation-text-title').innerText = ""
        result = true
    } else {
        result = false
        document.getElementById('validation-text-title').innerText = "Լրացրու դաշտը"
    }
    if (desc.length > 10) {
        document.getElementById('validation-text-desc').innerText = ""
        result = true
    } else {

        result = false
        document.getElementById('validation-text-desc').innerText = "Լրացրու դաշտը"
    }
    return result;
}
function htmltype(json) {
    $html = '<div class="note-item all-notes" id="trash">' +
        '<div class="note-inner-content">' +
        '<div class="note-content">' +
        '<p class="note-title" data-noteTitle="">' + json.title + '</p>' +
        '<p class="meta-time">' + json.dateTime + '</p>' +
        '<div class="note-description-content">' +
        '<p class="note-description" data-noteDescription="">' + json.description + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="note-action">' +
        '</div>' +
        '</div>' +
        '</div> ';

    $("#ct").prepend($html);
    $('#notesMailModal').modal('hide');
    var title = document.getElementById('n-title').value=' '
    var desc = document.getElementById('n-description').value=' '
    var date = document.getElementById('n-date').value=' '
    console.log(json)
}
function datatrash(data) {
    console.log(data)
    $('#trash'+data).remove();
}
