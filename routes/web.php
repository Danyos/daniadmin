<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::redirect('/home', '/admin');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/notes', 'HomeController@index')->name('home');
    Route::resource('LessonTimeList', 'School\LessonTimeFolderController');
    Route::resource('StudentList', 'School\StudentController');
    Route::post('LessonTimeList/createNew', 'School\LessonTimeFolderController@addLesson')->name('LessonTimeList.addLesson');
    Route::resource('notes', 'Notes\NotesController');
    Route::post('notes/update/type', 'Notes\NotesController@update')->name('notes.update.type');
    Route::get('notes/fav/type/', 'Notes\NotesController@fav')->name('notes.fav.type');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');
});
