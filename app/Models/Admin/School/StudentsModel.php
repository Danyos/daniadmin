<?php

namespace App\Models\Admin\School;

use Illuminate\Database\Eloquent\Model;

class StudentsModel extends Model
{
    protected $fillable=[
        'group_id',
        'fullName',
        'email',
        'age',
        'phone',
        'price',
        'start_date',

    ];
    public function StudentPrice()
    {
        return $this->hasOne(StudentPriceListMath::class,'student_id','id');
    }
}
