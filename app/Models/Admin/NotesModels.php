<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class NotesModels extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'dateTime',
        'type',
        'favourites',

    ];
}
