<?php

namespace App\Http\Controllers\Admin\School;

use App\Http\Controllers\Controller;
use App\Models\Admin\School\StudentPriceListMath;
use App\Models\Admin\School\StudentsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $a = StudentsModel::create($request->all());
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $student = StudentsModel::find($id);
        if (isset($request->priceall)) {


            StudentPriceListMath::create([
                'student_id' => $id,
                'status' => 'active',
                'price' => $student->price,
                'day' => 12,
                'price_full' => $student->price,
                'start_date' => Carbon::now()->format('Y-m-d'),

            ]);

            return back()->with('price_debt', ["id" => $student->group_id, "price_debt" => 'Վճարված է']);

        } elseif ($request->price != null) {


            $gumar = $student->price / 12;
            $day = explode('.', $request->price / $gumar + 0.3);

            StudentPriceListMath::create([
                'day' => $day[0],
                'student_id' => $id,
                'price' => $request->price,
                'price_full' => $request->price,
                'start_date' => Carbon::now()->format('Y-m-d'),
            ]);

            return back()->with('price_debt', ["id" => $student->group_id, "price_debt" => 'Վճարված է']);

        } else {
            return back()->with('price_debt', ["id" => $student->group_id, "price_debt" => 'Չկա գործողություն']);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
