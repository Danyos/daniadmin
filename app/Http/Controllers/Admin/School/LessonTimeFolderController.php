<?php

namespace App\Http\Controllers\Admin\School;

use App\Http\Controllers\Controller;
use App\Models\Admin\School\LessonTimeFolder;
use App\Models\Admin\School\StudentPriceListMath;
use App\Models\Admin\School\StudentsModel;
use App\User;
use Illuminate\Http\Request;

class LessonTimeFolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::first();
        $student=StudentsModel::with(['StudentPrice'])->where('group_id',$folderFirst->id)->get();
        $user=User::get();

        return view('admin.School.LessonTimeList.index',compact('folder','student','folderFirst','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::find($request->folder);
        $student=StudentsModel::where('group_id',$folderFirst->id)->get();

        return redirect()->route('admin.LessonTimeList.show',$request->folder);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user=User::get();
        $folder=LessonTimeFolder::get();
        $folderFirst=LessonTimeFolder::find($id);
        $student=StudentsModel::where('group_id',$folderFirst->id)->get();

        return view('admin.School.LessonTimeList.index',compact('folder','student','folderFirst','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if(isset($request->priceall)){

            $student=StudentsModel::find($id);
            $studentprice= StudentPriceListMath::where('status','inactive')->where('student_id',$id)->get();
            foreach ($studentprice as $studentprices){

                $studentprices->update([
                    'status'=>'active',
                    'price'=>$student->price,
                    'day'=>12,
                    'price_full'=>$student->price,

                ]);
            }
            return back()->with('price_debt',["id"=>$student->group_id,"price_debt"=>'Վճարված է']);

        }elseif ($request->price!=null){
            $student=StudentsModel::find($id);
            $studentprice= StudentPriceListMath::where('status','inactive')->where('student_id',$id)->where('parent_id',null)->first();
            $gumar=$student->price/12;
          $day=explode('.',$request->price/$gumar+0.3);

            StudentPriceListMath::create([
                'day'=>$day[0],
                'parent_id'=>$studentprice->id,
                'student_id'=>$id,
                'price'=>$request->price,
                'price_full'=>$request->price+$studentprice->price_full,
            ]);
            $j=$request->price+$studentprice->price_full;

            $day2=explode('.',$j/$gumar);

            $studentprice->update([
                'price_full'=>$request->price+$studentprice->price_full,
                'day'=>$day2[0]
            ]);
            return back()->with('price_debt',["id"=>$student->group_id,"price_debt"=>'Վճարված է']);

        }else {

            $student = StudentsModel::find($id);
            $studentpriceall = StudentPriceListMath::where('student_id', $id)->where('status', 'inactive')->get();
            $studentprice = StudentPriceListMath::where('student_id', $id)->where('status', 'inactive')->where('parent_id', '=', null)->first();
            if ($studentprice) {
                $s = $studentprice->price_full;

                if ($s >= $student->price) {
                    foreach ($studentpriceall as $studentprices) {

                        $studentprices->update([
                            'day'=>12,
                            'status' => 'active'
                        ]);
                    }


                    return back()->with('price_debt',["id" => $student->group_id, "price_debt" => 'Ամիսը լրացավ']);
                } else {

                    $k = $student->price - $s;

                    return back()->with('price_debt',["id" => $student->group_id, "price_debt" => $k]);
                }
            }

        else{
            return back()->with('price_debt',["id" => $student->group_id, "price_debt" => 'Չկա գործողություն']);}
    }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addLesson(Request $request)
    {
      LessonTimeFolder::create($request->all());

        return back();



    }
}
